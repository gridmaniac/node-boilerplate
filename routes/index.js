var express = require('express');
var router = express.Router();
var xlsx = require('node-xlsx');
var mongoose = require('mongoose');

var model = require('../models/model').Model;
mongoose.connect('mongodb://localhost:27017');

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home Page' });
});

module.exports = router;
