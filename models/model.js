var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: {type: String, trim: true},
    any: {type: String, trim: true}
}, {
    collection: 'models'
});

exports.Model = mongoose.model('Model', schema);